# 'Population map for the EU28 + Switzerland, Norway and Iceland for the year 2012'


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the population distribution in EU28 on hectare (ha) level. 

For the population distribution we draw on data for the European population in 2006 on the level of 1km² [1]. Another available dataset for the population in 2014 on the level of 250 x 250 m [2] turned out to be less reliable then the coarser one.
However, [2] partly covers area, which is not covered by [35]. Therefore, we used a calculated data  set  for  the  population  on  1km²  level.  If  our  primary  population  layer  indicated  no  population on 1km², we used the layer derived from [1] as a fall‐back option. An analysis of the resulting quality of the combined layer indicated that: 


- The combination actually improves the quality in those areas, where the primary population layer has uncovered areas
- It also tends to introduce a bias in less densely populated areas as a (non‐systematic) shift in  the  (1  x  1km²)  grid  cells  between  the  primary  and  secondary  population  layer  can  be  observed in many regions.  

This has the effect that when applying the so derived population distribution function on the population data on the NUTS3 level, the distribution is distorted towards rural areas. To reduce this adverse effect, the population of the fall‐back option is weighted with a factor of 30%.  
Within the 1 km² grid cells we used the information of the Corine land cover data [3] and the European Settlement Map layer on the 10 x 10 m level [4] to distribute the population to the hectare level.

Using this data, we calculate the population for the local administrative units (LAU). According to [5] these are around 115 thous. and regions (using the LAU2, except for Greece and Latvia, where we used LAU1). We then compared the so derived population with the population in local administrative units of statistical data sources ([6], [7], [8]). Then we adjust the local weighing factor of the population to find  a compromise between  the population on 1 km²,  population per LAU region, as well as upper limits for the population density per hectare level. For the upper limit we analysed the 95%/99%‐percentile of the ratio of population compared to the population in the corresponding 1 x 1 km grid cell (for all cells, which exceed a mean ratio by a factor of 2), clustered by the population densities on the 1 km² grid level. Based on this analysis we define an upper limit for the population on a hectare level.

The heated gross floor area will be used in the Hotmaps toolbox to estimate the heating demand in both residential and service sector.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3 page 35.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.
For the residential heated gross floor area, statistical data are available for most countries on the level of NUTS3. Again, manually performed data quality checks indicate that results are plausible on the hectare level of most regions. However, as of now, we do not factor in the fact, that the heat area per inhabitant often decrease with an increasing population density. For  NUTS3  regions  with  a  strong  urban  versus  rural  area  gradient,  this  might  lead  to  overestimation of the heated residential gross floor area in urban areas. Regarding the heated gross floor area of non‐residential buildings, data sources are even uncertain on the NUTS0 level. Data quality checks indicate that the sum of residential and non‐ residential heated gross floor area are plausible as well as the ratio between residential and non‐residential gross floor area,  even  though  the  later  indicator  might  not  hold  for  grid  cells which contain only  few  buildings.


### References
[35] [Open Street Map (OSM).](www.openstreetmap.org)

[36] [European Settlement Map.](https://land.copernicus.eu/pan-european/GHSL/european-settlement-map/esm-2012-release-2017-urban-green/view)

[37] [Corine Land Cover (CLC) 2012, Version 18.5.1.](https://land.copernicus.eu/pan-european/corine-land-cover/clc-2012/view)

[2] [European Settlement Map.](https://land.copernicus.eu/pan-european/GHSL/european-settlement-map/esm-2012-release-2017-urban-green/view)

[38] [Communes ‐ Eurostat.](http://ec.europa.eu/eurostat/web/gisco/geodata/reference‐data/administrative‐units‐statistical‐units/communes)

[39] Joint Research Center, “Estimation of the Gross Domestical Product 2006 in the 119 000 LAU2 of the ESPON Area.” ESPON, 2011.

[40] Eurostat, “Correspondence table LAU 2 – NUTS 2010, EU‐27.” Eurostat, 2010.

[30] [CensusHub2 - 2011 Census database.](https://ec.europa.eu/CensusHub2/query.do?step=selectHyperCube&qhc=false.)


## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Mu¨ller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Ku¨hnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2020: Andreas Mueller, Mostafa Fallahnejad
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

